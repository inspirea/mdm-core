package mdmcore

import (
	"os"

	"github.com/op/go-logging"
)

type MDMLog struct {
	log            *logging.Logger
	format         logging.Formatter
	backendConsole *logging.LogBackend
}

func GetLogger() *MDMLog {
	bl := new(MDMLog)
	bl.log = logging.MustGetLogger("bis")
	bl.format = logging.MustStringFormatter(
		`%{color}%{time:15:04:05.000} %{level:.4s} | %{shortfunc} | %{color:reset} %{message}`,
	)
	bl.backendConsole = logging.NewLogBackend(os.Stderr, "", 0)
	backendFormatter := logging.NewBackendFormatter(bl.backendConsole, bl.format)
	logging.SetBackend(backendFormatter)

	return bl
}

var LOG = GetLogger().log

func (bl *MDMLog) Info(message string) {
	bl.log.Info(message)
}

func (bl *MDMLog) Debug(message string) {
	bl.log.Debug(message)
}

func (bl *MDMLog) Error(message string) {
	bl.log.Error(message)
}

func (bl *MDMLog) Critical(message string) {
	bl.log.Critical(message)
}

func (bl *MDMLog) Warning(message string) {
	bl.log.Warning(message)
}

func (bl *MDMLog) Panic(message string) {
	bl.log.Panic(message)
}

func (bl *MDMLog) Fatal(message string) {
	bl.log.Fatal(message)
}

func (bl *MDMLog) Infof(message string, args ...interface{}) {
	bl.log.Infof(message, args)
}

func (bl *MDMLog) Debugf(message string, args ...interface{}) {
	bl.log.Debugf(message)
}

func (bl *MDMLog) Errorf(message string, args ...interface{}) {
	bl.log.Errorf(message)
}

func (bl *MDMLog) Warningf(message string, args ...interface{}) {
	bl.log.Warningf(message)
}

func (bl *MDMLog) Panicf(message string, args ...interface{}) {
	bl.log.Panicf(message)
}

func (bl *MDMLog) Fatalf(message string, args ...interface{}) {
	bl.log.Fatalf(message)
}
